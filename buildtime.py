#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import collections
import apt_pkg
import pprint
import functools
import math
import random

import torch
import torch.nn
import torch.optim
import torch.utils.data

Sample = collections.namedtuple('Sample', ['arch', 'package', 'dist', 'version', 'timestamp', 'status', 'time', 'space', 'builder'])

@functools.total_ordering
class VersionNum(object):
    def __init__(self, version):
        self.version = version

    def __str__(self):
        return "VersionNum({})".format(self.version)

    def __repr__(self):
        return "VersionNum({})".format(self.version)

    def __lt__(self, other):
        return apt_pkg.version_compare(self.version, other.version) < 0

    def __eq__(self, other):
        return self.version == other.version

    def __hash__(self):
        return hash(self.version)

def read_csv():
    # Parse all input
    available_versions = {}
    all_samples = []
    for line in sys.stdin:
        line = line.strip()
        sample = Sample(*line.split(','))
        try:
            sample = sample._replace(time=float(sample.time), space=float(sample.space), version=VersionNum(sample.version))
        except ValueError:
            continue
        if sample.time <= 0.0:
            continue
        if sample.package not in available_versions:
            available_versions[sample.package] = set()
        available_versions[sample.package].add(sample.version)
        all_samples.append(sample)

    # Choose latest version for each package
    chosen_version = {}
    for package in available_versions:
        chosen_version[package] = max(available_versions[package])

    # Select the considered sample list
    packages = set()
    builders = set()
    samples = []
    for sample in all_samples:
        if sample.package in chosen_version and chosen_version[sample.package] == sample.version:
            assert sample.time != 0.0
            assert sample.builder != ''
            samples.append(sample)
            packages.add(sample.package)
            builders.add("{}/{}".format(sample.arch, sample.builder))
    packages = list(packages)
    builders = list(builders)
    packages.sort()
    builders.sort()

    packages_idxs = dict([(package, idx) for idx, package in enumerate(packages)])
    builders_idxs = dict([(builder, idx) for idx, builder in enumerate(builders)])

    print("There are {} samples, {} packages and {} builders, for an average of {} samples per package and {} samples per builder"
          .format(len(samples), len(packages), len(builders), len(samples) / len(packages), len(samples) / len(builders)))

    inputs = []
    outputs = []
    random.shuffle(samples)
    for sample in samples:
        inputs.append((packages_idxs[sample.package], builders_idxs["{}/{}".format(sample.arch, sample.builder)]))
        outputs.append(sample.time)

    return inputs, outputs, packages_idxs, builders_idxs

class Net(torch.nn.Module):

    def __init__(self, num_packages, num_builders):
        super(Net, self).__init__()
        self.num_packages = num_packages
        self.num_builders = num_builders

        self.packages = torch.nn.Parameter(2.0 * torch.ones(num_packages, 1))
        self.builders = torch.nn.Parameter(torch.zeros(num_builders, 1))
        self.bias = torch.nn.Parameter(torch.zeros(1, 1))

    def forward(self, x):
        idxs_package = x[:, 0]
        idxs_builder = x[:, 1]
        return torch.exp(torch.index_select(self.packages, 0, idxs_package) - torch.index_select(self.builders, 0, idxs_builder))# + self.bias

    def normalize(self):
        factor = sum(self.builders).item() / len(self.builders)
        #factor = max(self.builders).item()
        self.builders.data -= factor
        self.packages.data -= factor

PRINT_LOSS_EVERY = 100

def save_model(net, packages_idxs, builders_idxs):
    with open('model-packages.txt', 'w') as fout:
        for package in packages_idxs:
            value = net.packages.data[packages_idxs[package], 0]
            fout.write("{}: {}\t\t\t# ~{} seconds\n".format(package, value, math.exp(value)))
    with open('model-builders.txt', 'w') as fout:
        for builder in builders_idxs:
            value = net.builders.data[builders_idxs[builder], 0]
            fout.write("{}: {}\n".format(builder, value))

def main():
    apt_pkg.init()
    inputs, outputs, packages_idxs, builders_idxs = read_csv()
    net = Net(len(packages_idxs), len(builders_idxs))

    samples_num = len(inputs)
    train_num = int(samples_num * 0.9)

    trainloader = torch.utils.data.DataLoader(torch.utils.data.TensorDataset(torch.tensor(inputs[:train_num]), torch.tensor(outputs[:train_num])), shuffle=True, batch_size=100)
    testloader = torch.utils.data.DataLoader(torch.utils.data.TensorDataset(torch.tensor(inputs[train_num:]), torch.tensor(outputs[train_num:])), shuffle=True, batch_size=100)

    criterion = torch.nn.MSELoss()
    #optimizer = torch.optim.SGD(net.parameters(), lr=0.001)
    optimizer = torch.optim.RMSprop(net.parameters(), lr=0.001)
    sched = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min', verbose=True)

    for epoch in range(10000):
        running_loss = 0.0
        for i, data in enumerate(trainloader):
            ins, outs = data
            optimizer.zero_grad()
            #print(net.packages.data.grad)
            act_outs = net(ins)
            #pprint.pprint(act_outs[:, 0].log())
            #pprint.pprint(outs)
            loss = criterion(act_outs[:, 0].log(), outs.log())
            loss.backward()
            optimizer.step()

            net.normalize()

            running_loss += loss.item()
            if i % PRINT_LOSS_EVERY == PRINT_LOSS_EVERY-1:
                print('[{}, {}] loss: {}'.format(epoch+1, i+1, running_loss / PRINT_LOSS_EVERY))
                #pprint.pprint(net.packages)
                #pprint.pprint(net.builders)
                running_loss = 0.0

        test_loss = 0.0
        for i, data in enumerate(testloader):
            ins, outs = data
            act_outs = net(ins)
            loss = criterion(act_outs[:, 0].log(), outs.log())
            test_loss += loss
        test_loss /= len(testloader)

        print("Test loss: {}".format(test_loss))
        sched.step(test_loss)

        save_model(net, packages_idxs, builders_idxs)

if __name__ == '__main__':
    main()
